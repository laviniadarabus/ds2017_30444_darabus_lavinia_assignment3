package consumer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.TimeoutException;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;

import org.apache.commons.lang3.SerializationUtils;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Envelope;

import messageHandling.Receive;
import models.Dvd;

public class Receiver extends Receive {

	ArrayList<String> emails;

	public Receiver(String endpointName) throws IOException, TimeoutException {
		super(endpointName);
		this.emails = new ArrayList<String>();
	}

	@Override
	public void handleConsumeOk(String consumerTag) {
		System.out.println("Consumer " + consumerTag + " registered");
	}

	@Override
	public void handleDelivery(String consumerTag, Envelope env, BasicProperties props, byte[] body)
			throws IOException {
		// TODO Auto-generated method stub
		Dvd dvd = (Dvd) SerializationUtils.deserialize(body);
		File f = new File(dvd.getTitle() + ".txt");
		FileWriter writer = new FileWriter(f);
		writer.write(dvd.toString());
		writer.close();
		String from = "laviniadarabus@gmail.com";
		String host = "smtp.gmail.com";
		Properties properties = System.getProperties();
		properties.setProperty(host, "587");
		Session session = Session.getDefaultInstance(properties);
		f = new File("emails.txt");
		FileReader reader = new FileReader(f);
		BufferedReader bufreader = new BufferedReader(reader);
		String email;
		while ((email = bufreader.readLine()) != null) {
			MimeMessage message = new MimeMessage(session);

			// Set From: header field of the header.
			try {
				message.setFrom(new InternetAddress(from));
				// Set To: header field of the header.
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));

				// Set Subject: header field
				message.setSubject("New dvd");

				// Now set the actual message
				message.setText("Hello, a new dvd has become available at our shop: " + dvd.toString());

				// Send message
				Transport.send(message);
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		bufreader.close();
		reader.close();
		System.out.println(dvd.getTitle());
	}

}
