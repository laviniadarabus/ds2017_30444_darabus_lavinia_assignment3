package messageHandling;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.ShutdownSignalException;

public class Receive extends EndPoint implements Consumer{

	public Receive(String endpointName) throws IOException, TimeoutException {
		super(endpointName);
		// TODO Auto-generated constructor stub
	}

	public void receive() throws IOException {
		channel.basicConsume(endPointName, true, this);
	}

	public void handleCancel(String arg0) throws IOException {
		// TODO Auto-generated method stub
		
	}

	public void handleCancelOk(String arg0) {
		// TODO Auto-generated method stub
		
	}

	public void handleConsumeOk(String arg0) {
		// TODO Auto-generated method stub
		
	}

	public void handleDelivery(String arg0, Envelope arg1, BasicProperties arg2, byte[] arg3) throws IOException {
		// TODO Auto-generated method stub
		
	}

	public void handleRecoverOk(String arg0) {
		// TODO Auto-generated method stub
		
	}

	public void handleShutdownSignal(String arg0, ShutdownSignalException arg1) {
		// TODO Auto-generated method stub
		
	}
	
	

}
