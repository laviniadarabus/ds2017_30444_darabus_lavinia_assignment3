package messageHandling;

import java.io.IOException;
import java.io.Serializable;
import java.util.concurrent.TimeoutException;

import org.apache.commons.lang3.SerializationUtils;

public class Send extends EndPoint{
	

	public Send(String endPointName) throws java.io.IOException, TimeoutException {
		super(endPointName);
	}
	
	public void sendMessage(Serializable object) throws IOException {
		channel.basicPublish("", endPointName, null, SerializationUtils.serialize(object));
	}

}
